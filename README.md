# Reading

## Reading Challenge Books to Read later
- Banned Book Read - Tropic of Cancer
- Book of Fiction - Fight Club
- Book to Reread - The Obstacle is the way
- Books Reccomended by people I look up to:
  - Vagabonding by Rolf Potts
  - Drive by Daniel Pink
  - Give and Take by Adam Grant
  - Atomic Habits
  - Inner Engineering
- Review a book like a critic
  - ?????
- Memorize a passage - ?????
- Reach out to an author - ryan holiday
- Read a Difficult Book - War and Peace Tolstoy
- Art of Computer Programming Knuth

## Reading List
- Meditations
- Barbarian Days
- Crime and Punishment
- Book of 5 Rings
- The Art of Learning
- Books by Seneca
- Stephan Hawking - Big Answers to Brief Questions
- Letters from a Stoic - Seneca
- The Essential Epicurius - Epicuius
- [Practical Philosophy List](https://ryanholiday.net/a-practical-philosophy-reading-list/)
- Empire of the Summer Moon
- No Ordinary Dog
- War of Art
- Zen and Art of Archery
- Tribe of Mentor
- The Plague by Albert Camus
- Wealth of Nations
- Emotional Agility
- Fluent Forever
- The Quiet Professional
- The oxygen advantage
- Senteniel - Pat Mac
- Stillness is the key
- Poor charles almanack (Rec by naval)
- La casa de los espiritus
- Made to stick
- Artists way
- Mindful Athlete

## Reading List for Writing
- Hey whipple squeeze this
- Olgivy on ads
- Writing down the bones
